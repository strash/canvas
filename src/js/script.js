// var can, ctx, array = [];;
// can = document.getElementById('myCanvas');
// ctx = can.getContext('2d');

// canvas-@2x
function canvas2x (can) {
  'use strict';
  can.style.width = can.width + 'px';
  can.style.height = can.height + 'px';
  can.width = can.width * 2;
  can.height = can.height * 2;
}

// генератор прямоугольников
function generateColoredBlocks(arr, imgWidth, imgHeight) {
  'use strict';
  var canPixelWidth, canPixelHeight, xOffset, yOffset;
  canPixelWidth = can.width / imgWidth; //коэффициент ширины пикселя (ширина канваса / количество пикселов по ширине)
  canPixelHeight = can.height / imgHeight;
  xOffset = 0;
  yOffset = 0 - canPixelHeight;

  for (var i = 0; i < imgWidth * imgHeight; i++) {
    arr.push(randomRGB());
  }

  for (var j = 0; j < arr.length; j++) {
    ctx.fillStyle = arr[j];
    ctx.fillRect(
      xOffset === can.width ? xOffset = 0 : xOffset,
      xOffset === 0 ? yOffset += canPixelHeight : yOffset,
      canPixelWidth, canPixelHeight
    );
    xOffset += canPixelWidth;
  }
  return arr;
}

// среднее цветовое значение для генератора прямоугольников
function averageBlocks(arr) {
  'use strict';
  var str, r, rsum = 0, g, gsum = 0, b, bsum = 0, j = '';

  for (var i = 0; i < arr.length; i++) { //перебираем весь массив
    str = arr[i].slice(4, arr[i].length - 1); //обрезаем 'rgb(' и ')'
    for (var k = 0; k < str.length; k++) {
      if (str[k] == ',') j += k;
      if (j.length == 1) r = Number(str.slice(0, j));
      if (j.length == 2) {
        g = Number(str.slice(Number(j[0]) + 1, j[1]));
        b = Number(str.slice(Number(j[1]) + 1, str.length));
        j = '';
      }
    }
    rsum += r;
    gsum += g;
    bsum += b;
  }

  rsum = Math.round(rsum / arr.length);
  gsum = Math.round(gsum / arr.length);
  bsum = Math.round(bsum / arr.length);

  arr.length = 0;

  var rgb = 'rgb(' + rsum + ',' + gsum + ',' + bsum + ')';
  return rgb;
}

// document.querySelector('.average').onclick = function () {
//   generateColoredBlocks(array, 3, 2);
//   this.style.background = averageBlocks(array);
// };

/* — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — — */

/*
 * Нахождение всех изображений
 */
function getImagesSrc(img) {
  'use strict';
  var arr = [];
  for (var i = 0; i < img.length; i++) {
    arr[i] = img[i].src;
  }
  return arr;
}

/*
 * Среднее цветовое значение из изображения
 * param node (img)
 * return Array [r, g, b, a]
 */
function averageImage(imgsrc) {
  'use strict';
  var img, myimg, r, g, b, a, counter = 0;
  var canvas, context;
  canvas = document.createElement('canvas');
  canvas.id = 'canvas';
  context = canvas.getContext('2d');
  document.body.appendChild(canvas);

  img = new Image();

  img.src = imgsrc;
  context.drawImage(img,0,0);

  myimg = context.getImageData(0, 0, img.width, img.height).data;

  r = g = b = a = 0;
  for (var i = 0; i < myimg.length; i += 4) { //перебираем весь массив
    if (myimg[i + 3] > 0) {
      r += myimg[i + 0]; /* r,g,b 0–255*/
      g += myimg[i + 1];
      b += myimg[i + 2];
      a += myimg[i + 3]; /* alfa 0–255, но не 0–1 или 0–100 */
      counter++;
    }
  }
  r = Math.round(r / counter);
  g = Math.round(g / counter);
  b = Math.round(b / counter);
  a = a / counter / 255;

  document.body.removeChild(document.getElementById('canvas'));

  var rgba = [r, g, b, Math.round(a * 100) / 100];
  return rgba;
}

// test
function printColorValue(myParentNode, text) {
  'use strict';
  var p = document.createElement('p');
  p.className = 'parag';
  myParentNode.appendChild(p);
  p.textContent = text;
}

function decreaseS(s) {
  'use strict';
  return s > 0.5 ? s = 0.5 : s;
}

function decreaseV(v) {
  'use strict';
  return v > 0.8 ? v = 0.8 : v;
}

function setShadow(x, y, blur, spread) {
  'use strict';
  var imgParent = document.querySelectorAll('.imgblock'),
      images = document.querySelectorAll('.image'),
      r, g, b, a,
      h, s, v;

  for (var i = 0; i < getImagesSrc(images).length; i++) {
    r = averageImage(getImagesSrc(images)[i])[0];
    g = averageImage(getImagesSrc(images)[i])[1];
    b = averageImage(getImagesSrc(images)[i])[2];
    a = averageImage(getImagesSrc(images)[i])[3];

    h = rgbToHsv(r, g, b)[0];
    s = rgbToHsv(r, g, b)[1];
    v = rgbToHsv(r, g, b)[2];

    r = Math.round(hsvToRgb(h, decreaseS(s), decreaseV(v))[0]);
    g = Math.round(hsvToRgb(h, decreaseS(s), decreaseV(v))[1]);
    b = Math.round(hsvToRgb(h, decreaseS(s), decreaseV(v))[2]);

    images[i].setAttribute('style', 'box-shadow:' +x+ ' ' +y+ ' ' +blur+ ' ' +spread+ ' ' + 'rgba(' +r+ ',' +g+ ',' +b+ ',' +a+ ')');

    // printColorValue(imgParent[i], 'RGBA '  +r+ ', ' +g+ ', ' +b+ ', ' +a+ ' == HSB ' +h+ ', ' +s+ ', ' + decreaseV_Value(v));
  }
}

window.onload = function () {
  setShadow('0px', '25px', '35px', '-15px');
  // canvas2x(can);
  // getImages(document.querySelectorAll('.image'));
  // ctx.fillStyle = averageImage('http://music.strash.ru/images/covers/Instrumental_Mixtape_2.jpg');
  // document.querySelector('.background').style.background = averageImage(ctx, 'http://music.strash.ru/images/covers/A_Thousand_Suns.jpg');
  // ctx.fillRect(0,0,can.width,can.height);
};
