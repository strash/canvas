/* * * * * * *
 * VARIABLES
 * * * * * * */

var gulp = require('gulp'),
    haml = require('gulp-ruby-haml'),
    sass = require('gulp-sass'),
    htmlmin = require('gulp-html-minify'),
    cssmin = require('gulp-csso'),
    postcss = require('gulp-postcss'),
    jsmin = require('gulp-uglify'),
    imgmin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    advpng = require('imagemin-advpng'),
    jpegRecompress = require('imagemin-jpeg-recompress'),
    concat = require('gulp-concat'),
    gzip = require('gulp-gzip'),
    autoprefixer = require('autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    watch = require('gulp-watch'),
    gutil = require('gulp-util'),
    filesize = require('gulp-filesize'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean');

var path = {
  src: {
    haml: 'src/haml/*.haml',
    js: ['src/js/01_*/*.js', 'src/js/02_*/*.js', 'src/js/03_*/*.js', 'src/js/04_*/*.js', 'src/js/**/*.js', 'src/js/05_*/*.js'],
    sass: 'src/sass/main.sass',
    img: 'src/img/**/*.*'
  },
  build: {
    html: 'build',
    js: 'build/js',
    css: 'build/css',
    img: 'build/img'
  },
  watch: {
    haml: 'src/**/*.haml',
    js: 'src/js/**/*.js',
    sass: 'src/sass/**/*.sass',
    img: 'src/img/**/*.*'
  },
  clean: {
    html: 'build/*.html',
    css: 'build/css/**/*.*',
    js: 'build/js/**/*.*',
    img: 'build/img/**/*.*'
  }
};

/* * * * * * *
 * TASKS BUILD
 * * * * * * */

gulp.task('html', function () {
  gulp.src(path.src.haml)
    .pipe(haml().on('error', function(e) { console.log(e.message); }))
    .pipe(filesize())
    .pipe(htmlmin())
    .pipe(gulp.dest(path.build.html))
    .pipe(gzip())
    .pipe(filesize());
});

gulp.task('css', function () {
  gulp.src(path.src.sass)
    .pipe(sass().on('error', sass.logError))
    .pipe(filesize())
    .pipe(cssmin())
    .pipe(postcss([ autoprefixer({ browsers: ['last 5 versions'] }) ]))
    .pipe(rename('./style.min.css'))
    .pipe(gulp.dest(path.build.css))
    .pipe(gzip())
    .pipe(filesize());
});

gulp.task('js', function () {
  gulp.src(path.src.js)
    .pipe(jsmin())
    .pipe(concat('./script.min.js'))
    .pipe(gulp.dest(path.build.js))
    .pipe(filesize())
    .on('error', gutil.log);
});

gulp.task('img', function () {
  gulp.src(path.src.img)
    .pipe(imgmin({
      progressive: true,
      arithmetic: true,
      interlaced: true,
      optimizationLevel: 5,
      svgoPlugins: [
        {cleanupAttrs: true},
        {removeDoctype: true},
        {removeXMLProcInst: true},
        {removeComments: true},
        {removeMetadata: true},
        {removeTitle: true},
        {removeDesc: true},
        {removeUselessDefs: true},
        {removeXMLNS: true},
        {removeEditorsNSData: true},
        {removeEmptyAttrs: true},
        {removeHiddenElems: true},
        {removeEmptyText: true},
        {removeEmptyContainers: true},
        {removeViewBox: false},
        {cleanUpEnableBackground: true},
        {minifyStyles: false},
        {convertStyleToAttrs: false},
        {convertColors: true},
        {convertPathData: true},
        {convertTransform: true},
        {removeUnknownsAndDefaults: true},
        {removeNonInheritableGroupAttrs: true},
        {removeUselessStrokeAndFill: true},
        {removeUnusedNS: true},
        {cleanupIDs: true},
        {cleanupNumericValues: true},
        {cleanupListOfValues: true},
        {moveElemsAttrsToGroup: true},
        {moveGroupAttrsToElems: false},
        {collapseGroups: true},
        {removeRasterImages: true},
        {mergePaths: true},
        {convertShapeToPath: true},
        {sortAttrs: true},
        {transformsWithOnePath: true},
        {removeDimensions: false},
        {removeAttrs: false},
        {removeElementsByAttr: true},
        {addClassesToSVGElement: false},
        {addAttributesToSVGElement: false},
        {removeStyleElement: true}],
      use: [
        pngquant({
          floyd: 1,
          quality: '50-80',
          speed: 2}),
        advpng({optimizationLevel: 4}),
        jpegRecompress({
          quality: 'low',
          method: 'smallfry',
          min: 30, max: 90,
          loops: 10})]
    }))
    .pipe(gulp.dest(path.build.img));
});

/* * * * * * *
 * TASKS DEV
 * * * * * * */

gulp.task('htmlDev', function () {
  return gulp.src(path.src.haml)
    .pipe(haml().on('error', function(e) { console.log(e.message); }))
    .pipe(gulp.dest(path.build.html));
});

gulp.task('cssDev', function () {
  return gulp.src(path.src.sass)
    .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(postcss([ autoprefixer({ browsers: ['last 5 versions'] }) ]))
      .pipe(rename('./style.min.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.css));
});

gulp.task('jsDev', function () {
  return gulp.src(path.src.js)
    .pipe(sourcemaps.init())
      .pipe(concat('./script.min.js', {newLine: '\n\n//SPLICE\n\n\n'}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.js))
    .on('error', gutil.log);
});

gulp.task('clean', function () {
  gulp.src(path.clean.html, {read: false})
    .pipe(clean());
  gulp.src(path.clean.css, {read: false})
    .pipe(clean());
  gulp.src(path.clean.js, {read: false})
    .pipe(clean());
  gulp.src(path.clean.img, {read: false})
    .pipe(clean());
});

/* * * * * * *
 * WATCHER
 * * * * * * */

gulp.task('watch', function(){
    watch([path.watch.haml], function(event, cb) {
        gulp.start('htmlDev');
    });
    watch([path.watch.sass], function(event, cb) {
        gulp.start('cssDev');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('jsDev');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('img');
    });
});

/* * * * * * *
 * DEFAULT TASK
 * * * * * * */

gulp.task('dev', ['htmlDev', 'cssDev', 'jsDev', 'img', 'watch']);
gulp.task('default', ['html', 'css', 'js', 'img']); //build
